# Zombieproof Piglin

Datapack for Minecraft 1.16+ that makes Piglins that pick up a Totem of Undying immune to zombification.
